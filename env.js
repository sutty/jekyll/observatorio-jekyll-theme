---
---

window.env = {
  AIRBRAKE_PROJECT_ID: {{ site.env.AIRBRAKE_PROJECT_ID | default: 0 }},
  AIRBRAKE_PROJECT_KEY: '{{ site.env.AIRBRAKE_PROJECT_KEY }}',
  JEKYLL_ENV: '{{ site.env.JEKYLL_ENV }}',
  SPREE_URL: '{{ site.env.SPREE_URL }}'
};

{% comment %}
Genera un site.json con las traducciones del sitio y los datos de los
pasos del carrito.  No los extraemos directamente con el filtro
`jsonify` porque extraen demasiada información y el JSON se rompe.
{% endcomment %}
{%- assign steps = 'cart,shipment,payment,confirmation' | split: ',' -%}

window.site = {
  {%- for step in steps -%}
  "{{ step }}": {
    {%- for attribute in site.data.layouts[step] -%}
    {%- assign attribute_key = attribute[0] -%}
    "{{ attribute_key }}": {{ site[step][attribute_key] | jsonify }}{% unless forloop.last %},{% endunless %}
    {%- endfor -%}
  },
  {%- endfor -%}
  "i18n": {{ site.i18n | jsonify }}
};

{%- comment -%}
  Para agregar plantillas que se procesan con JS, las agregamos en
  Liquid dentro de assets/templates/ y luego las importamos acá, de forma
  que estén disponibles para JS sin tener que descargarlas.

  Es importante que la plantilla esté envuelta por el tag `raw`,
  para que no sea procesado en el momento de generar env.js, sino en el
  navegador.
{%- endcomment -%}
{%- capture alert %}{% include_relative assets/templates/alert.html %}{% endcapture -%}
{%- capture results %}{% include_relative assets/templates/results.html %}{% endcapture -%}
{%- capture cart %}{% include_relative assets/templates/cart.html %}{% endcapture -%}
{%- capture payment_methods %}{% include_relative assets/templates/payment_methods.html %}{% endcapture -%}
{%- capture recover_order %}{% include_relative assets/templates/recover_order.html %}{% endcapture -%}
{%- capture shipping_methods %}{% include_relative assets/templates/shipping_methods.html %}{% endcapture -%}

window.templates = {
  "alert": {{ alert | jsonify }},
  "results": {{ results | jsonify }},
  "cart": {{ cart | jsonify }},
  "payment_methods": {{ payment_methods | jsonify }},
  "recover_order": {{ recover_order | jsonify }},
  "shipping_methods": {{ shipping_methods | jsonify }},
};
