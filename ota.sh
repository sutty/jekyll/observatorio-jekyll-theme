#!/bin/sh
set -e

# El primer parámetro tiene que ser el sitio
test -n "$1"

echo "Entrar al repositorio ${1}" >&2
cd "/srv/http/_sites/${1}"

echo "Traer cambios, moviendo los cambios locales arriba de todo en la historia (rebase)" >&2
su -c 'git pull --rebase' app

echo "Subir archivos binarios" >&2
su -c 'git add public && git commit -m public' app || true

echo "Enviar cambios locales" >&2
su -c 'git push' app

test -z "${2}" && exit 0

cd /srv/http
su -c "bundle exec rails r 'puts \"Publicando cambios en $1\"; site = Site.find_by_name \"${1}\"; DeployJob.perform_now site.id'" app
