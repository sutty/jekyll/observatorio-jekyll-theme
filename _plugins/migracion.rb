# frozen_string_literal: true

Jekyll::Hooks.register :site, :post_read, priority: :high do |site|
  next unless ENV['JEKYLL_ENV'] == 'migration'

  require 'securerandom'
  require 'jekyll-write-and-commit-changes'

  docs = site.documents
  biblioteca = %w[biblioteca_externa].freeze
  post = %w[evento opinion page biblioteca prensa].freeze

  save = {}

  docs.each do |doc|
    layout = doc.data['layout']
    uuid = doc.data['uuid']

    unless uuid
      uuid = doc.data['uuid'] = SecureRandom.uuid
      save[uuid] ||= doc
    end

    doc.data['categories'] ||= []

    if post.include? layout
      doc.data['layout'] = 'post'
      doc.data['categories'] << layout.tr('_', ' ').capitalize
      save[uuid] ||= doc
    end

    if biblioteca.include? layout
      doc.data['layout'] = 'biblioteca'
      doc.data['categories'] << layout.tr('_', ' ').capitalize
      save[uuid] ||= doc
    end

    path = doc.data.dig('file', 'path')

    if path && !File.exist?(path)
      Jekyll.logger.warn "#{path} no existe"
    end
  end

  save.values.each(&:save)
  exit
end
